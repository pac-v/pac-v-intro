##  Pakistan Against COVID19 Volunteers PAC-Volunteers (PAC-V):

PAC-V is a group of volunteers from across Pakistan, which has come together to develop affordable solutions to combat COVID19. The volunteers numbering over 150 are doctors, biomedical professionals, engineers, academics, diaspora, resource mobilizers, and other smaller groups. The group's active collaboration is currently focused on developing local design and engineering solutions to make available the following equipment

 - low-cost and massively available respiratory ventilators, portable oxygen supplies,
 - face masks and protection screens,
 - respiratory valves,
 - viral media tubes
 - non-contact thermometers,
 - retrofitting existing vents to serve multiple patients;
 - and arranging 3d printing farms, manufacturing and funding support across the nation to deal with the COVID19 crisis

## Contacts:

 1. Dr. Bilal Siddiqui (PAC-V Initiative Lead)
+923203461598
[airbilal@gmail.com](mailto:airbilal@gmail.com)
 2. Fouad Bajwa (Media Team)
+92333466120
[fouadbajwa@gmail.com](mailto:fouadbajwa@gmail.com)
 3. Salman Khan (Media Team)
+923212400355
[salmanuiux@gmail.com](mailto:salmanuiux@gmail.com)
